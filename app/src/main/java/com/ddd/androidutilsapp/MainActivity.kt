package com.ddd.androidutilsapp

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ddd.androidutils.DoubleClick
import com.ddd.androidutils.DoubleClickListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val doubleClick = DoubleClick(object : DoubleClickListener {
            override fun onSingleClickEvent(view: View?) {
                Toast.makeText(baseContext, "Yeah One click", Toast.LENGTH_SHORT).show()
            }

            override fun onDoubleClickEvent(view: View?) {
                Toast.makeText(baseContext, "Yeah Two clicks", Toast.LENGTH_SHORT).show()
            }
        })

        click.setOnClickListener(doubleClick)
    }
}
