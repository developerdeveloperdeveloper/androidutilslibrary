# AndroidUtilsLibrary <br> [![GitLab release](https://img.shields.io/static/v1?label=Release&message=1.0.0&color=Blue)](https://gitlab.com/developerdeveloperdeveloper/androidutilslibrary/-/tags/1.0.0)

AndroidUtilsLibrary is a collection of utils. 

## Installation

#### Gradle

###### 1. Add the JitPack repository to your build file, in your root build.gradle at the end of repositories:

```bash
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```

###### 2. Add the dependency

```bash
dependencies {
    implementation 'com.gitlab.developerdeveloperdeveloper:androidutilslibrary:1.0.0'
}
```

#### Maven

###### 1. Add the JitPack repository to your build file

```bash
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>
```

###### 2. Add the dependency

```bash
<dependency>
    <groupId>com.gitlab.developerdeveloperdeveloper</groupId>
    <artifactId>androidutilslibrary</artifactId>
    <version>1.0.0</version>
</dependency>
```

## Usage

```kotlin
import com.ddd.androidutils.DoubleClick
import com.ddd.androidutils.DoubleClickListener

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val doubleClick = DoubleClick(object : DoubleClickListener {
            override fun onSingleClickEvent(view: View?) {
                Toast.makeText(baseContext, "Yeah One click", Toast.LENGTH_SHORT).show()
            }


            override fun onDoubleClickEvent(view: View?) {
                Toast.makeText(baseContext, "Yeah Two clicks", Toast.LENGTH_SHORT).show()
            }
        })

        click.setOnClickListener(doubleClick)
    }
}
```
## Contacts

|Name|Personal Repository|
|----|-------------------|
|ArcaDan|[@ArcaDan](https://gitlab.com/ArcaDan)|
|Barros9|[@Barros9](https://github.com/Barros9)|

## License
Icons made by <a href="https://www.flaticon.com/authors/creaticca-creative-agency" title="Creaticca Creative Agency">Creaticca Creative Agency</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
